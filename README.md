# MUI
Minimalistic, scalable UI mod aimed at experienced players.

## Download
The latest stable release and more vivid descriptions can be found at [PaydayMods](http://paydaymods.com/mods/44/arm_mui) or [Steam Community](http://steamcommunity.com/app/218620/discussions/15/620713633850948964/).

## Testers
* Ciggy Bumma
* Doctor Ironic
* Flex Pank
* TheAntiYou567
* Bint Chiko
* Tom Servo